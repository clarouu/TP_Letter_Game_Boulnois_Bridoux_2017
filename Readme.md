# TP Architecture Logicielle / Inf4043 - 2017 - Jeux de lettres / BOULNOIS - BRIDOUX 

# Explication du jeu effectué

Pour comrendre plus en détail les différentes fonctions du jeu, se reporter aux comentaires inclus dans le code.
Le projet est composé de plusieurs classes regroupées par package : 

# Les composantes du jeu :
* Le Joueur qui est caractérisé par un nom, un tableau de mots réalisés et un drapeau 
permettant de savoir s'il souhaite passer son tour ou non.
Ce joueur aura la possibilité de réaliser plusieurs actions représentées ici par des méthodes telles 
que ajouter/supprimer un mot dans son tableau, vérifier si le mot qu'il vient de faire se trouve 
déjà dans son tableau, vérifier que le mot qu'il vient de faire se trouve dans un tableau d'un autre
joueur, piocher une lettre au hasard, savoir s'il est le gagnant, s'il veut passer son tour, etc...

* L'IA qui hérite de la classe Joueur, a donc accès à ces méthodes. Dans sa classe on y touve principalement 
la méthode qui permet de créer un mot. Elle a alors accès au dictionnaire afin de créer un mot. On va lire le 
dictionnaire mot par mot et regarder si les lettres dans le mot lu sont aussi dans le pot commun. Si c'est le cas,
on récupérera ce mot pour le jouer. Tant que l'IA trouve des mots à faire elle le fera, sinon elle passera son tour
en retournant le mot 'quit'.
La seconde méthode permet de mettre à jour le tableau qui nous permet de vérifier si une lettre du pot commun 
a déjà été utiisée pour créer un mot. 

# Le déroulement du jeu :
* Le Jeu 
Cette classe prend en compte le nombre de joueurs, les joueurs eux-même qui sont rénunis dans une ArrayList.
Nous y trouvons également le pot commun qui est repésenté par une ArrayList de caractères, ainsi qu'un
drapeau nous prévenant de l'arrêt du jeu, c'est-à-dire, lorqu'un joueur a gagné.
Cette classe permet entre autre d'afficher les joueurs, le pot commun, d'ajouter/supprimer une lettre 
du pot commun, vérifier si une lettre existe dans celui-ci. Cette dernière fonctionalité est très utile
lorsque l'on veut supprimer les lettres piochées pour constituer un mot. 
Cette classe permet également de mettre à jour le flag permettant de savoir si le jeu est terminé ou non,
et de limiter le nombre de joueurs.

* Le Tour hérite de la classe Jeu. Elle comporte deux méthodes. L'une permet au premier joueur de tirer 
deux lettres et de les mettre dans le pot commun et aux autres joueurs de tirer une lettre et de 
la mettre dans le pot commun. L'autre donne l'id du premier joueur à jouer. 

# Le lancement du jeu :
* Le Main propose tout d'abord un menu à deux choix : Joueur à plusieurs ou contre l'IA.
Après ce choix le nombre de joueurs est demandé si le premier choix a été sélectionné, sinon il est automatiquement
initialisé à deux joueurs (l'IA et le joueur), puis leurs noms sont demandés (dans le cas du second choix, l'IA 
est automatiquement ajouté dans le tableau de joueurs). Les joueurs sont alors enregistrés dans l'ArrayList de 
la classe Jeu. 
Ensuite on détermine l'id du premier joueur en faisant tirer une lettre
au hasard et en regardant qui a la lettre la plus pettite. 
Le jeu peut alors commencer : c'est le tour du joueur, représenté ici par une boucle while qui est imbriquée
dans une autre boucle while qui vérifie si nous ne sommes pas à la fin du jeu (avec un booléen). 
Dans la boucle représentant le tour d'un joueur nous allons lui demander s'il souhaite faire un mot ou 
s'il souhaite passer son tour. S'il s'ait de l'IA qui joue, nous invoquerons la fonction lui permettant de créer un mot.
S'il a décidé de faire un mot, nous vérifions si ce mot fait partie du
dictionnaire, s'il a déjà été fait ou non (donc s'il appartient à son propre dictionnaire ou au dictionnaire
d'un autre joueur. Si tel est le cas, nous supprimons ce mot du tableau du joueur). 
S'il préfère passer son tour, on passe la main au joueur suivant dans l'ArrayList.

# Le dictionnaire du jeu : 
* Le dictionnaire et l'interface associée qui contient une methode permettant d'ouvrir, 
lire le fichier représentant le dictionnaire et de vérifier 
si le mot réalisé existe bien dans notre dictionnaire. 

# Pour compiler sous linux
* Pour exécuter le jeu utiliser la commande : java -jar "TP_Letter_Game_Boulnois_Bridoux.jar" (envoyé par mail)
* Pour exécuter les tests maven : se déplacer dans le dossier TP_Letter_Game_Boulnois_Bridoux_2017/LetterGame/,
lancer la commande suivante : mvn compile puis mvn test






# Consignes du projet
- Date de rendu : 12/02/2017 23h - pas de retard accepté
- Binôme accepté
- Contacts : 
  - mlab.cours[at]gmail[dot]com (TP à rendre à cette adresse)
  - ledoyen.esiea[at]gmail[dot]com


## Règles du jeux 

- Objectif du jeux :
  - Le premier joueur ayant 10 mots gagne la partie

- Déroulement du jeux :
  - Chacun des joueurs tire une lettre aléatoire d'un sac, et les mettent au milieu dans le pot commun
  - Le joueur qui a tiré la lettre la plus petite lettre dans l'alphabet commence
  - Chaque fois que c'est le début du tour d'un joueur il tire deux lettres aléatoires qu'il rajoute au pot commun
  - Chaque fois qu'un joueur fait un mot il tire une lettre aléatoire qu'il rajoute au pot commun
  - Quand le joueur ne trouve plus de mot il passe et le joueur suivant commence son tour (par tirer 2 lettres qu'il rajoute au pot commun)

- Comment faire un mot ?
  - En utilisant uniquement les lettres du pot commun
  - En prenant un mot de ces adversaires (toutes les lettres du mot) et en lui rajoutant des lettres du pot commun
  - En rallongeant un de ces mots avec des lettres du pot commun ou en utilisant un autre mot (toutes les lettres)
  - Attention, seul les noms communs sont autorisés

- Pour faciliter :
  - les lettres possibles sont uniquement les 26 de l'alphabet (ex : é <-> e)
  - les mots composés sont considérés comme deux mots

- Pour les plus avancés :
  - Le cas des anagrammes :
    - On peut voler un mot en faisant un anagramme uniquement si il n'a pas déjà été fait. Bien entendu, faire un anagramme permet de tirer une nouvelle lettre.

## Objectif du TP

- Une première étape consiste à pouvoir jouer à plusieurs autour d'un même écran.
- Une interface en ligne de commande est suffisante.
- Nous attendons aussi a minima une de ces deux extensions (ou les deux pour les plus courageux :-)) :
  - Une architecture client / serveur, chaque joueur utilisant une instance d'un client pour jouer.
  - Une intelligence artificiel permettant de jouer contre l'ordinateur.
- Nous attendons aussi une description de votre architecture (Quel responsabilité à chaque package, ..).
- De plus, vous devrez illustrer trois principes SOLID ou design pattern en utilisant vos propres classes. 
  - pourquoi avez-vous utilisé ce design pattern / principe ? Qu'est-ce que cela vous a apporté ? Comment l'avez-vous appliqué ?
  - Nous attendons quelques paragraphes seulement
- Ces deux exercices sont à livrer dans le README.md du projet.

## Technologies à utiliser 

- Le TP devra être rendu sur github et donc être gérer via Git
  - Plusieurs commits par personne sont attendus! 
- Le projet doit être rendu en Java. 
- Le projet devra pouvoir être compiler et lancer en ligne de commande (sans IDE) :
  - L'utilisation de `Maven` ou `Gradle` est recommandée 
  - cela ne sert a rien de commiter une jar. Nous n'exécuterons que du code compilé par nous même.
- Le projet doit contenir des tests unitaires
  - Utilisation de `JUnit` ou `Test-ng`
  - L'utilisation des librairies comme `assertJ` et `Mockito` est recommandé.

## Comment rendre son TP

- Merci d'envoyer **un mail dont le sujet est `nom_binome1 | nom_binome2 | url_github`**
  - Tout non respect de cette règle entrainera un 0 au TP (Un script récupérera les projets)
- Tout les exercices demandés (autre que le code) sont à livrer au [format markdown](https://guides.github.com/features/mastering-markdown/) dans un README.md à la racine de votre projet.
  - Le README.md peut référencer d'autres fichiers markdown situés dans le projet 

- Une grande importance sera attachée à la qualité du code, à la conception objet et au découpage par fonctionnalités avec des contrats clairs. 
- Nous vous encourageons à utiliser des analyseurs de code statiques (PMD, findbugs, ...). Nous les utiliserons pour corriger.
- Nous encourageons aussi une approche TDD sur le projet. 

## Points d'attention lors de la correction

- Respect des consignes de rendu de projets
- Des explications claires et fonctionnelles pour compiler et lancer le projet situé dans le README.md. 
- Barèmes :

| Points | Description           | 
| :----- |:-------------| 
|5 points | Architecture du code, découpage des classes, respect des principes Objects (SOLIDE), méthodes < 15 lignes... |
|5 points | La totalité des feature faites. Pas de bug et cas aux limites gérés  |
|3 points | Test : code coverage > 70%, assertions intelligentes dans les tests , tests unitaires |
|2 point  | Exercice Architecture & Design Pattern / Solid |
|2 points | Analyse statique de code findbug / PMD |
|2 point  | Utilisation de Maven (ou autre logiciel du même type) pour gérer les dépendances et construire le projet. Utilisation de git avec plusieurs commits pour chaque personnes du binome |
|1 point  | Conventions java / Maven respectées (Camelcase, package, ...) |

## Bootstrap du projet

Pour faciliter le début nous vous proposons :

```
$ git clone https://github.com/MLabusquiere/TP_4A_2017_Letter_Game.git LetterGame
$ cd LetterGame
$ git remote rm origin
$ git remote add origin <your_git_repository_url>
$ git push -u origin master
```
