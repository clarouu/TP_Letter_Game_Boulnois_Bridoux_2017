package fr.esiea.unique.binome.name.dictionary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Dictionary implements IDictionary{

	/**Methodes**/
	
	public boolean isWordInDico(String mot){
		String fichier ="src/main/resources/dico.txt";
		boolean flag = false;
		
		//lecture du fichier texte	
		try{
			InputStream ips=new FileInputStream(fichier); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null){
				if(ligne.equals(mot)){
					flag = true;
				}
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		return flag;
	}
	
}
