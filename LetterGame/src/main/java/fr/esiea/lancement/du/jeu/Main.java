package fr.esiea.lancement.du.jeu;

import java.util.Scanner;

import fr.esiea.composante.du.jeu.*;
import fr.esiea.deroulement.du.jeu.*;
import fr.esiea.unique.binome.name.dictionary.Dictionary;

public class Main {
	public static void main(String[] args) {
		
		Jeu jeu = new Jeu();
		Joueur joueur = new Joueur();
		Joueur joueurPlaying = new Joueur();
		Dictionary dico = new Dictionary();
		int nb = 0;
		boolean flag = false;
		IA ia = new IA();
		Scanner sc = new Scanner(System.in);
		
		//Menu et initialisation des joueurs
		System.out.println("BIENVENUE DANS LE GRAND JEU DES LETTRES !");
		System.out.println("Veuillez selectionner :\n1 - Jouer à plusieurs\n2 - Jouer contre l'IA");
		while(flag != true){
			int choix = sc.nextInt();
			if(choix == 1){
				System.out.println("Entrez le nombre de joueurs : ");
				nb = sc.nextInt();
				jeu.setNbJoueur(nb);
				sc.nextLine();
				System.out.println("Quels sont vos petits noms ..?");
				askNames(jeu, sc, 0);
				flag = true;
			}
			else if(choix == 2){
				nb = 2;
				jeu.setNbJoueur(nb);
				jeu.addPlayer(ia);
				sc.nextLine();
				askNames(jeu, sc, 1);
				flag = true;
			}else{
				System.out.println("Veuillez sélectionner '1' ou '2' svp.");
			}
		}
	
		//Savoir qui va jouer en premier
		int i = 1;
		Tour tour = new Tour();
		int idFirstPlayer = tour.firstToPlay(jeu, joueur, i);
		joueurPlaying = jeu.getJoueur(idFirstPlayer);
		String mot = "";
		String exit = "quit";
		
		//Tant que le jeu n'est pas fini
		while(jeu.getGameIsOver() != true){
			joueurPlaying = jeu.getJoueur(idFirstPlayer);
			tour.nextPlayer(jeu, idFirstPlayer);
			
			//Tant que le joueur ne passe pas son tour
			while (joueurPlaying.getTourOver()!=true){
				jeu.afficherPotCommun();
				System.out.println("Quel mot souhaites-tu faire " + joueurPlaying.getName() + " ?");
				System.out.println("(Pour passer votre tour entrez 'quit')");
				if(joueurPlaying.getName() == "IA"){
					mot = ia.createWord(jeu.getPotCommun(), dico);
				}else{
					mot = sc.nextLine();	
				}
				System.out.println(mot);
				//On vérifie que le joueur veut passer son tour ou non 
				if(mot.equals(exit)){
					//On passe le tour...
					joueurPlaying.setTourOver(true);
					//...au suivant
					if(idFirstPlayer == (jeu.getNbJoueur()-1)){
						idFirstPlayer = 0;
					}
					else{
						idFirstPlayer +=1;
					}
				}
				else {
					if(dico.isWordInDico(mot) == true){
						if(joueurPlaying.getName() == "IA"){
							//On met à jour le tableau idLetterPc
							ia.updateIdLetterPc(jeu.getPotCommun());
						}
						//On vérifie que le mot n'a pas déjà été fait
						if(joueurPlaying.isTheWordBelongToMyTab(mot) == false){
							int j = 0;
							//On vérifie si le joueur courant veut piquer un mot d'un autre joueur 
							while(j != (jeu.getNbJoueur())){
								joueur = jeu.getJoueur(j);
								if(joueur.isTheWordBelongToAnotherTab(mot) != "" && joueur != joueurPlaying){
									joueur.deleteWordInTab(joueur.isTheWordBelongToAnotherTab(mot));
								}
								j++;
							}
							//On ajoute le mot fait par le joueur courant dans son tableau
							joueurPlaying.addWordInTab(mot);
							//On affiche de quoi est composé le tableau du joueur en cours
							System.out.print("Ton tableau est maintenant composé de : ");
							joueurPlaying.getTab();
							//On supprime les lettres du pot commun utilisées
							jeu.deleteInPotCommun(mot);
							//Le joueur tire une nouvelle lettre et la met dans le pot commun
							jeu.addToPotCommun(joueurPlaying.getRandomLetter());
							//On vérifie si le joueur a dix mots dans son tableau
							if(joueurPlaying.isWinner() == true){
								//Je quitte d'abord la boucle de mon tour pour...
								joueurPlaying.setTourOver(true);
								//...quitter la boucle du jeu
								jeu.setGameIsOver(true);
							}
						}
						else{
							System.out.println("Dsl, vous avez déjà fait ce mot !");
						}
					}
					else{
						System.out.println("Dsl, ce mot n'existe pas !");
					}
				}
				
			
			}
		}
		System.out.println("FELICITATION ! " + joueurPlaying.getName() + " a gagné !");
		
	}

	private static void askNames(Jeu jeu, Scanner sc, int i) {
		String name;
		Joueur joueur;
		while(i != (jeu.getNbJoueur())){
			System.out.println("Nom du joueur " + i + " :");
			name = sc.nextLine();
			jeu.addPlayer((new Joueur()));
			joueur = jeu.getJoueur(i);
			joueur.setName(name);
			joueur.setTourOver(false);
			jeu.setJoueur(i,joueur);
			i++;
		}
	}
}
