package fr.esiea.composante.du.jeu;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import fr.esiea.unique.binome.name.dictionary.Dictionary;

public class IA extends Joueur{
	/**Attributs**/
	
	//tableau qui va indiquer si j'ai déjà utilisé une lettre dans le pot commun ou non
	//pour pouvoir créer un mot
	private ArrayList<Integer> idLettersPc = new ArrayList<Integer>();

	/**Methodes**/
	
	public String createWord(ArrayList<Character> potCommun, Dictionary dico){
		int i,j, cpt = 0;
		String word = "";
		boolean flag = false;
		
		while(flag != true){
			String fichier ="src/main/resources/dico.txt";
			
			//lecture du fichier texte	
			try{
				InputStream ips=new FileInputStream(fichier); 
				InputStreamReader ipsr=new InputStreamReader(ips);
				BufferedReader br=new BufferedReader(ipsr);
				String ligne;
				char[] mot = null;
				for(i = 0; i<potCommun.size(); i++){
					idLettersPc.add(1);
				}
				//On lit les mots du dico
				while ((ligne=br.readLine())!=null){
					mot = ligne.toCharArray();
					//On ré-initialise le tableau qui nous permet de savoir si j'ai déjà utilisé une lettre ou non
					for(i = 0; i<potCommun.size(); i++){
						idLettersPc.set(i, 1);
					}
					//On ré-initialise le compteur pour un nouveau mot
					cpt = 0;
					//On lit chaque lettre de chaque mot
					for(j = 0; j<mot.length; j++){
						//On lit chaque lettre du pot commun
						for(i = 0; i<potCommun.size(); i++){
							if(mot[j] == potCommun.get(i) && idLettersPc.get(i) == 1){
								cpt++;
								idLettersPc.set(i, 0);
							}
							//Si on a trouvé des lettres dans le pot commun qui sont
							//aussi dans un mot du dico, on le garde 
							if(mot.length == (cpt-1)){
								word = ligne;
								flag = true;
							}
						}
					}
				}
				br.close();
				//Pour sortir de la recherche de mot (avec les lettres du pot commun) 
				//quoiqu'il arrive (par ex, on n'a pas assez de lettre pour faire un mot)
				flag = true;
			}		
			catch (Exception e){
				System.out.println(e.toString());
			}
			
		}
		//Si je n'ai pas réussi à faire de mot, je passe mon tour
		if(word == ""){
			word = "quit";
		}
		
		return word;
	}
	
	public void updateIdLetterPc(ArrayList<Character> potCommun){
		while(idLettersPc.size() < potCommun.size()){
			idLettersPc.add(1);
		}
		while(idLettersPc.size() > potCommun.size()){
			idLettersPc.remove(1);
		}
	}
	
}
