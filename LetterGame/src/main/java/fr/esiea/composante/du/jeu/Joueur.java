package fr.esiea.composante.du.jeu;

import java.util.ArrayList;
import java.util.Random;

import fr.esiea.deroulement.du.jeu.Jeu;

public class Joueur {
	
	/**Attributs**/
	
	private String nom;
	private ArrayList<String> tab = new ArrayList<String>();
	private boolean tourOver;
	
	/**Constructeur**/
	public Joueur(){
		nom = "IA";
	}
	
	/**Methodes**/
	
	public void setTourOver(boolean flag){
		tourOver = flag;
	} 
	
	public boolean getTourOver(){
		return tourOver;
	}
	
	//Pour pouvoir afficher le nom du joueur concerne
	public String getName(){
		return nom;
	}
	
	public int getSizeTab(){
		return tab.size();
	}
	
	//Pour initialiser le nom du joueur au commencement du jeu
	public void setName(String name){
		nom = name;
	}
	
	//On affiche le tableau (pour que les autres joueurs le voient)
	public void getTab(){
		int i = 0;
		for(i = 0; i<tab.size(); i++){
			System.out.println(tab.get(i) + " ");
		}
	}
	
	//Quand un autre joueur souhaite recuperer un mot que je possède afin de le modifier et l'acquerir
	public boolean isTheWordBelongToMyTab(String word){
		int i = 0;
		boolean flag = false; 
		for(i = 0; i<tab.size(); i++){
			if(tab.get(i) == word){
				flag = true;
			}
		}
		return flag;
	}
	
	public String isTheWordBelongToAnotherTab(String word){
		int i = 0;
		int j = 0;
		int k = 0;
		int l = 0;
		String wordToDelete = "";
		//char[] wordInMyTab = null;
		char[] mot = word.toCharArray();
		int cpt = 0;
		ArrayList<Integer> id = new ArrayList<Integer>();
		
		//Je regarde chaque mot de mon tableau
		while(i<tab.size()){
			//Je récupère le mot que j'étudie dans un tableau de caractères
			char[] wordInMyTab = tab.get(i).toCharArray();
			//Je ré-initialise mon tableau d'id et mon compteur
			updateId(wordInMyTab, id);
			for(l = 0; l<wordInMyTab.length; l++){
				id.add(1);
			}
			cpt = 0;
			//Je regarde chaque lettre du mot que l'adversaire veut faire
			for(j = 0; j<mot.length; j++){
				//Je regarde chaque lettre du mot que je lis dans mon tableau
				for(k = 0; k<wordInMyTab.length; k++){
					if(mot[j] == wordInMyTab[k] && id.get(k) == 1){
						cpt++;
						id.set(k, 0);
					}
				}
			}
			//Si toutes les lettres de ce mot sont utilsés aussi pour faire le mot
			//que le joueur courant veut faire, on va lui piquer son mot
			if(cpt == mot.length){
				wordToDelete = tab.get(i);
				//Pour quitter la boucle while
				i = tab.size();
			}
			i++;
		}
		return wordToDelete;
	}
	
	private void updateId(char[] wordInMyTab, ArrayList<Integer> id){
		while(id.size() < wordInMyTab.length){
			id.add(1);
		}
		while(id.size() > wordInMyTab.length){
			id.remove(1);
		}
	}
	
	//Lorsque le joueur reussi a trouver un mot, on l'ajoute a son tableau
	public void addWordInTab(String word){
		tab.add(word);
	}
	
	//Supprimer un mot qu'un joueur a recupere de mon tableau
	public void deleteWordInTab(String word){
		int i = 0;
		for(i = 0; i<tab.size(); i++){
			if(tab.get(i) == word){
				tab.remove(i);
			}
		}
	}
	
	//Savoir si le joueur a gangé
	public boolean isWinner(){
		boolean flag = false;
		if(tab.size() == 10){
			flag = true;
		}
		return flag;
	}
	
	//Permet de tirer une lettre au hasard
	public char getRandomLetter(){
		Random rand = new Random();
		char c = (char)(rand.nextInt(26) + 97);
		return c;
	}
	

}
