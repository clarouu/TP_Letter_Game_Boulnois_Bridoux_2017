package fr.esiea.deroulement.du.jeu;

import fr.esiea.composante.du.jeu.Joueur;

public class Tour extends Jeu{
	
	/**Attributs**/
	
	private Joueur first;
	private int idFirst;
	
	/**Methodes**/
	
	/*Permet juste au premier joueur de tirer 2 lettres et de les mettre dans le 
	pot commun et aux autres joueurs de tirer 1 lettre et de la mettre dans le pot
	commun
	*/
	public void nextPlayer(Jeu jeu, int idFirst){
		int i = 0;
		char c;
		boolean flag = false;
		while(i != (jeu.getNbJoueur())){
			Joueur joueur = jeu.getJoueur(i);
			if(i == idFirst && flag == false){
				c = joueur.getRandomLetter();
				jeu.addToPotCommun(c);
				c = joueur.getRandomLetter();
				jeu.addToPotCommun(c);
				joueur.setTourOver(false);
				flag = true;
				i = 0;
			}
			else{
				c = joueur.getRandomLetter();
				jeu.addToPotCommun(c);
				i++;
			}
			
		}
	}
	
	//Donne l'id du premier joueur à jouer
	public int firstToPlay(Jeu jeu, Joueur joueur, int i){
		int joueurFirst = 0;
		Joueur joueur1 = jeu.getJoueur(0);
		char letterRandom1 = joueur1.getRandomLetter();
		jeu.addToPotCommun(letterRandom1);
		while(i != (jeu.getNbJoueur())){
			joueur = jeu.getJoueur(i);
			char letterRandom = joueur.getRandomLetter();
			if(letterRandom1<letterRandom){
				joueurFirst = i;
			}
			else{
				letterRandom1 = letterRandom;
				joueurFirst = i;
			}
			jeu.addToPotCommun(letterRandom);
			i++;
		}
		return joueurFirst;
	}
}
