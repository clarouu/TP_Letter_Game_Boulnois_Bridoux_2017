package fr.esiea.deroulement.du.jeu;
import java.util.ArrayList;
import java.util.Scanner;

import fr.esiea.composante.du.jeu.*;

public class Jeu {
	/**Attributs**/
	
	private int nbJoueur;
	private ArrayList<Joueur> tabJoueur = new ArrayList<Joueur>();
	private ArrayList<Character> potCommun = new ArrayList<Character>();
	private boolean gameIsOver = false;
	
	/**Methodes**/
	
	public void setNbJoueur(int nb){
		nbJoueur = nb;
	}
	
	public int getNbJoueur(){
		return nbJoueur;
	}
	
	public void addPlayer(Joueur joueur){
		tabJoueur.add(joueur);
	}
	
	public Joueur getJoueur(int indice){
		Joueur joueur = new Joueur(); 
		for(int i = 0; i < tabJoueur.size(); i++){
			if(i == indice){
				joueur = tabJoueur.get(i);
			}
		}
		return joueur;
	}
	
	public void setJoueur(int indice, Joueur joueur){
		for(int i = 0; i < tabJoueur.size(); i++){
			if(i == indice){
				tabJoueur.set(i, joueur);
			}
		}
	}
	
	public void afficherPotCommun(){
		int i = 0;
		System.out.println("Le pot commun est constitué de : ");
		for(i = 0; i<potCommun.size(); i++){
			System.out.println(potCommun.get(i) + " ");
		}
	}
	
	public int getSizePotCommun(){
		return potCommun.size();
	}
	
	public void addToPotCommun(char lettre){
		potCommun.add(lettre);
	}
	
	public ArrayList<Character> getPotCommun(){
		return potCommun;
	}
	
	public void deleteInPotCommun(String mot){
		int i,j = 0;
		boolean flag = false;
		char[] word = mot.toCharArray();
		for(j = 0; j<word.length; j++){
			flag = false;
			for(i = 0; i<potCommun.size(); i++){
				if(word[j] == potCommun.get(i) && flag == false){
					potCommun.remove(i);
					flag = true;
				}
			}
		}
	}
	
	public boolean isInPotCommun(char lettre){
		int i = 0;
		boolean flag = false;
		for(i = 0; i<potCommun.size(); i++){
			if(potCommun.get(i) == lettre){
				flag = true;
			}
		}
		return flag;
	}
	
	//Vérifier le nombre de joueur
	public void limitNumberOfPlayers(int num){
		while(num > 10){
			System.out.println("Vous êtes trop nombreux ! Veuillez diminuer votre groupe svp :");
			Scanner sc = new Scanner(System.in);
			setNbJoueur(sc.nextInt());
		}
	}
	
	public void setGameIsOver(boolean flag){
		gameIsOver = flag;
	}
	
	public boolean getGameIsOver(){
		return gameIsOver;
	}
}
