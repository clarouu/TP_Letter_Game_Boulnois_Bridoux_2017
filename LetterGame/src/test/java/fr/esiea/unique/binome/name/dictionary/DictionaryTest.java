package fr.esiea.unique.binome.name.dictionary;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test sample for Dictionary.
 */
public class DictionaryTest {

    private Dictionary dictionary = new Dictionary();

    @Test
    public void testIsWord() {
        if(dictionary.isWordInDico("maman") != true){
        	fail("Nous rencontrons un problème dans la lecture du dictionnaire.");
        }
    }
}
