package fr.esiea.composante.du.jeu;

import static org.junit.Assert.*;
import org.junit.Test;

import fr.esiea.composante.du.jeu.Joueur;
public class JoueurTest {

	private Joueur joueur = new Joueur();
	
	@Test
	public void randomLetterTest(){
		char c = joueur.getRandomLetter();
		if(c < 'a' || c > 'z'){
			fail("On a un caractère hors alphabet !");
		}
	}
	
	@Test
	public void addWordInTab(){
		int taille = joueur.getSizeTab();
		joueur.addWordInTab("test");
		if(joueur.getSizeTab() != taille+1){
			fail("La donnée a été mal enregistrée.");
		}
	}
	
	@Test
	public void deleteWordInTab(){
		int taille = joueur.getSizeTab();
		joueur.addWordInTab("test");
		joueur.deleteWordInTab("test");
		if(joueur.getSizeTab() != taille){
			fail("La donnée a été mal supprimée ou n'existe pas.");
		}
	}
}