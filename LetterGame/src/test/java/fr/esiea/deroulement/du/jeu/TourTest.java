package fr.esiea.deroulement.du.jeu;
import static org.junit.Assert.*;
import org.junit.Test;

import fr.esiea.composante.du.jeu.Joueur;
public class TourTest 
{
	private Tour tour = new Tour();
	private Jeu jeu = new Jeu();
	private Joueur joueur = new Joueur(); 
		 
	@Test 
	public void firstToPlay()
	{
		int i = 0;
		String name = "coco";
		jeu.setNbJoueur(3);
		while(i != 3){
			jeu.addPlayer((new Joueur()));
			joueur = jeu.getJoueur(i);
			joueur.setName(name);
			joueur.setTourOver(true);
			jeu.setJoueur(i,joueur);
			i++;
		}
		i=0;
		int idFirst = tour.firstToPlay(jeu, joueur, i);
		if(idFirst < 0 || idFirst >= jeu.getNbJoueur()){
			fail("Le premier joueur à jouer n'a pas été bien défini.");
		}
	}
}
