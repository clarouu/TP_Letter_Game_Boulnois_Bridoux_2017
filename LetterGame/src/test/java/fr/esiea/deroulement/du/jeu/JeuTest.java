package fr.esiea.deroulement.du.jeu;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.esiea.composante.du.jeu.Joueur;

public class JeuTest {
	
	private Joueur joueur = new Joueur();
	private Jeu jeu = new Jeu();
	
	@Test
	public void setJoueurTest(){
		jeu.addPlayer(joueur);
		joueur = jeu.getJoueur(0);
		joueur.setName("coco");
		jeu.setJoueur(0, joueur);
		joueur = jeu.getJoueur(0);
		if(joueur.getName() != "coco"){
			fail("Le nom du joueur a été mal enregistré");
		}
		
	}
	
	@Test
	public void addToPotCommunTest(){
		int size = jeu.getSizePotCommun();
		jeu.addToPotCommun('a');
		if(jeu.getSizePotCommun() != size+1){
			fail("La lettre n'a pas été ajoutée au pot commun.");
		}
	}
	
	@Test
	public void deleteInPotCommunTest(){
		int size = jeu.getSizePotCommun();
		jeu.addToPotCommun('a');
		jeu.deleteInPotCommun("a");
		if(jeu.getSizePotCommun() != size){
			fail("La lettre n'a pas été ajoutée au pot commun.");
		}
	}

}
